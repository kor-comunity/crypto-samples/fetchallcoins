const https = require("https");
const url = "https://api.coinpaprika.com/v1/coins";

exports.handler =  function(event, context, callback) {
  https.get(url, res => {
    res.setEncoding("utf8");
    let body = "";
    res.on("data", data => {
      body += data;
    });
    res.on("end", () => {
      body = body;      
      return JSON.parse(body);
    });
  });
}